﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Email_de_cobrança
{
    public class EnviaEmail
    {
       
        public string MailTo { get; set; }

        public string Assunto { get; set; }
        public string Body { get; set; }

        public MailPriority Prioridade { get; set; }

        public async Task<bool> Send()
        {

           // ConfigPadraoCasoVazio();
            using (var objEmail = new MailMessage())
            {
                objEmail.From = new MailAddress("postmaster@wmb.com.br", "No-Reply", System.Text.Encoding.UTF8);

                objEmail.Subject = Assunto;
                objEmail.SubjectEncoding = System.Text.Encoding.UTF8;
                objEmail.Body = Body;
                objEmail.BodyEncoding = System.Text.Encoding.UTF8;
                objEmail.IsBodyHtml = true;
                objEmail.Priority = Prioridade;

                if (!string.IsNullOrEmpty(MailTo))
                {
                    objEmail.To.Add(MailTo);

                }

                using (var objSmtp = new SmtpClient())
                {

                    objSmtp.Host = "smtp.office365.com";
                    objSmtp.EnableSsl = true;
                    objSmtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    objSmtp.Port = 587;
                    objSmtp.UseDefaultCredentials = false;
                    objSmtp.Credentials = new NetworkCredential("postmaster@wmb.com.br", "Ksa 56jh123!@#", "MicrosoftOffice365Domain.com");
                    await objSmtp.SendMailAsync(objEmail);
                }

                return true;
            }
            

        }


    }
}

