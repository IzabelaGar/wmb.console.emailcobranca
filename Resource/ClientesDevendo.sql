﻿/*Declare @data_ref varchar(100)
Set @data_ref = cast(YEAR(GETDATE()) as varchar(10)) +'/'+ cast(month(getdate()) as varchar(10)) + '/01'
*/


SELECT DISTINCT tbl_Admin.stremail, tbl_Admin.str_EMAIL2, tbl_Admin.strnome
FROM            tbl_boleto INNER JOIN
                         tbl_Admin ON tbl_boleto.int_IDC = tbl_Admin.intid INNER JOIN
                         tbl_CONFIG ON tbl_boleto.int_IDC = tbl_CONFIG.int_IDCLIENTE
WHERE        (tbl_boleto.sdt_DataReferencia = @data_ref) AND (tbl_boleto.int_Tipo > 1) AND (tbl_Admin.int_STATUS = 4) AND (NOT EXISTS
                             (SELECT        TOP (1) int_IDC
                               FROM            tbl_boleto AS t1
                               WHERE        (sdt_DataReferencia = @data_ref) AND (int_status IN (2, 3)) AND (int_IDC = tbl_boleto.int_IDC))) AND (tbl_CONFIG.sdt_firstMesCobrancaWMB < @data_ref) AND (tbl_boleto.int_IDC NOT IN
                             (SELECT        int_IDC
                               FROM            tbl_Cielo_Recorrencia))