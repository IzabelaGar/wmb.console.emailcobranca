﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Configuration;

namespace Email_de_cobrança
{
    class Program
    {
        static void Main(string[] args)
        {

            string strConn = ConfigurationManager.ConnectionStrings["ConnMegacorretor"].ConnectionString;
            SqlConnection conexaoBD = new SqlConnection(strConn);
            conexaoBD.Open();

            var dataRef = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 01);
            var query = Resource.Consultas.ClientesDevendo;

            var dapper = conexaoBD.Query<EmailViewModel>(query, new { @data_ref = dataRef }).ToList();

            EnviaEmail objEmail = new EnviaEmail();
            var mensagem = "Prezado  {NomeImob}. Bom dia! <BR> </br>Para sua comodidade, informamos que o vencimento da sua mensalidade do seu site é " + GetDiaSemanaPtBr() + " agora(dia 12). <BR> "
                + "<BR>Aproveitamos para sugerir que você coloque seus pagamentos conosco em débito automático no seu cartão de crédito.</br></br>Além de mais praticidade, você ainda ganha desconto! <a href='https://painel.wmb.com.br/admin/financeiro_grid.aspx'> Clique aqui </a> e cadastre-se agora mesmo!</br><BR>"
                + " Se você ja realizou o pagamento via boleto, agradecemos e pedimos que desconsidere este lembrete.<BR><BR>Atenciosamente,<BR><BR> Atendimento WMB<BR>[OBS: Essa mensagem foi gerada automaticamente pelo nosso sistema]";

            objEmail.Assunto = "Aviso de vencimento";

            //Gambis para testar, REMOVER PRODUCAO
            dapper.Add(new EmailViewModel
            {
                stremail = "suporte@wmb.com.br",
                strnome = "Izabela",
                str_EMAIL2 = "suporte@wmb.com.br"
            });

            foreach (var item in dapper)
            {
                objEmail.Body = mensagem.Replace("{NomeImob}", item.strnome);

                if (item.stremail != item.str_EMAIL2)
                {
                    if (!string.IsNullOrEmpty(item.stremail))
                    {
                        objEmail.MailTo = item.stremail;
                        objEmail.Send().Wait();
                    }

                    if (!string.IsNullOrEmpty(item.str_EMAIL2))
                    {
                        objEmail.MailTo = item.str_EMAIL2;
                        objEmail.Send().Wait();
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(item.stremail))
                    {
                        objEmail.MailTo = item.stremail;
                        objEmail.Send().Wait();
                    }
                }

                Console.WriteLine($"Enviando email para -> ({item.stremail})");
            }
        }

        private static string GetDiaSemanaPtBr()
        {
            var diaSemana = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 12).DayOfWeek;
            var diaSemanaPt = "";
            switch (diaSemana)
            {
                case DayOfWeek.Sunday:
                    diaSemanaPt = "domingo";
                    break;
                case DayOfWeek.Monday:
                    diaSemanaPt = "segunda";
                    break;
                case DayOfWeek.Tuesday:
                    diaSemanaPt = "terça";
                    break;
                case DayOfWeek.Wednesday:
                    diaSemanaPt = "quarta";
                    break;
                case DayOfWeek.Thursday:
                    diaSemanaPt = "quinta";
                    break;
                case DayOfWeek.Friday:
                    diaSemanaPt = "sexta";
                    break;
                case DayOfWeek.Saturday:
                    diaSemanaPt = "sabado";
                    break;
                default:
                    break;
            }

            return diaSemanaPt;
        }
    }
}
